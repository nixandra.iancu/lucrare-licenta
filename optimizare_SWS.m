 %parametri_SAE_pv_sra_c_1PI
% determinarea parametrilor RG-PID cu functia GA
Te=0.00025; N=20000;

LB=[0 0.001 0.01]';     % pentru MM al SWS liniar
UB=[150 0.001 0.25]'; % pentru MM al SWS liniar
nrparam=length(LB);

%Genetic Algorithm
%opts = gaoptimset('PlotFcns',{@gaplotbestf,@gaplotstopping},'PopInitRange',[LB';UB']);
%opts = gaoptimset(opts,'StallTimeLimit',500,'PopulationSize',500);
%opts = gaoptimset(opts,'Generations',550,'StallGenLimit',500);
%[thetha,fval,EXITFLAG] =ga(@(thetha) functie_SWS(thetha,N),nrparam,[],[],[],[],LB,UB,[],opts); %using Genetic Algorithms

%Particle Swarm Optimization
options = optimoptions('particleswarm','PlotFcns',@pswplotbestf,'Display', 'iter');
options = optimoptions(options,'SwarmSize',500,'HybridFcn',@fmincon);
[thetha,fval,EXITFLAG1] =particleswarm(@(thetha) functie_SWS(thetha,N),nrparam,LB,UB,options) %using Praticle Swarm Optimization
x0=[0 0.001 0.01];
%Pattern search
%options = optimoptions('patternsearch', 'PlotFcn', {@psplotbestf, @psplotmeshsize});
%[thetha,fval,EXITFLAG1] =patternsearch(@(thetha) functie_SWS(thetha,N),x0,[],[],[],[],LB,UB,options) %using Pattern search

%Simulated Annealing
%options = optimoptions('simulannealbnd', 'PlotFcn', {@saplotbestx,@saplotbestf,@saplotx,@saplotf});
%[thetha,fval,EXITFLAG1] =simulannealbnd(@(thetha) functie_SWS(thetha,N),x0,LB,UB,options) %using Simulated Annealing

