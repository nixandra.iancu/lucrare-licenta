function [best_solution, best_fitness] = greyWolfOptimizer(objFunc, lb, ub, num_wolves, max_iter)
    % Grey Wolf Optimizer Algorithm
    % objFunc: Objective function to be minimized
    % lb: Lower bounds for each dimension (array)
    % ub: Upper bounds for each dimension (array)
    % num_wolves: Number of wolves in the population
    % max_iter: Maximum number of iterations
    
    % Number of dimensions
    num_dimensions = numel(lb);
    
    % Initialize the positions of the wolves randomly
    wolves_positions = repmat(lb, num_wolves, 1) + rand(num_wolves, num_dimensions) .* repmat((ub - lb), num_wolves, 1);
    
    % Initialize the alpha, beta, and delta positions and corresponding fitness values
    alpha_pos = zeros(1, num_dimensions);
    alpha_fitness = inf;
    beta_pos = zeros(1, num_dimensions);
    beta_fitness = inf;
    delta_pos = zeros(1, num_dimensions);
    delta_fitness = inf;
    
    % Main loop
    for iter = 1:max_iter
        % Update alpha, beta, and delta positions
        for i = 1:num_wolves
            fitness = objFunc(wolves_positions(i,:));
            if fitness < alpha_fitness
                delta_fitness = beta_fitness;
                delta_pos = beta_pos;
                beta_fitness = alpha_fitness;
                beta_pos = alpha_pos;
                alpha_fitness = fitness;
                alpha_pos = wolves_positions(i,:);
            elseif fitness < beta_fitness
                delta_fitness = beta_fitness;
                delta_pos = beta_pos;
                beta_fitness = fitness;
                beta_pos = wolves_positions(i,:);
            elseif fitness < delta_fitness
                delta_fitness = fitness;
                delta_pos = wolves_positions(i,:);
            end
        end
        
        % Update the positions of the wolves
        a = 2 - iter * (2 / max_iter); % linearly decreasing parameter
        for i = 1:num_wolves
            r1 = rand(1, num_dimensions);
            r2 = rand(1, num_dimensions);
            A = 2 * a .* r1 - a;
            C = 2 * r2;
            D = abs(C .* alpha_pos - wolves_positions(i,:));
            X1 = alpha_pos - A .* D;
            X2 = beta_pos - A .* D;
            X3 = delta_pos - A .* D;
            wolves_positions(i,:) = (X1 + X2 + X3) / 3;
        end
        
        % Boundary handling
        wolves_positions = max(wolves_positions, lb);
        wolves_positions = min(wolves_positions, ub);
    end
    
    % Find the best solution and its fitness value
    best_fitness = alpha_fitness;
    best_solution = alpha_pos;
end
