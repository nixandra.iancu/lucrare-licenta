clear;
%M.C.C parameters
Tm=0.013;               %constanta de timp mecanica [s]
Ta=0.001;               %constanta de timp electrica [s]
Tu=0;                   %constanta de timp a actuatorului [s]  
Ra=3.75;                %rezistenta electrica [ohm]
Rt=2;                   %terminal resistance [ohm]  
KE=8;                   %constanta actuatorului 
Km=0.056;               %torque constant [N*m/A]
Ke=0.04945;             %voltage constant [V/rad/s]
alfa=0.5;               %alfa poate fi intre [0.3; 1]
Kf=alfa*3.57e-4;        %[N*m*s/rad]
KMi=1;                  %[V/A]
KMw=1;                  %[V/rad/s]

a=0.1;                  %parametru de transmitere care caracterizeaza reductorul de turatie
Vs=1;                   %viteza liniara prescrisa [m/s]  
C=1000;                 %constanta de elasticitate 
r0=0.01;                %raza initiala a tamburului 
l=0.07;                 %latimea tamburului
h=0.0001;               %grosimea foliei de alama
ro=8520;                %densitatea alamei  
pi=3.1416;
fh=25;                  %forta de reziztenta 
%fh=0;                  %forta de reziztenta doar pentru dezvoltarea observatorului sliding mode
JM=0.18e-4;             %momentul de inertie al rotorului m.c.c [Kg*m^2]
La=Ra*Ta;

%fuzzy
Ts=0.001;
Tm=0.01;
kPC=1;

% crearea semnalului SPAB
Te=0.00025;
Tsim=5;
tt=0:Te:Tsim-Te;tt=tt(:);
N=Tsim/Te;
comandaProces = idinput(N,'PRBS',[0 5.5*Te],[0 1]); %înainte era idinput(N,'PRBS',[0 6*Te],[0 1])
spab=comandaProces(:); % generation of persistently exciting input signal

%sim("mcc_sra_c.mdl")