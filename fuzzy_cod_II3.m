% Parametrii regulatorului Fuzzy
kr_II3=1/(kPC*Ts);
Tr_II3=Tm;
kc_II3=kr_II3*Tr_II3;
Ti_II3=Tr_II3;
kP_II3=(kc_II3*Te)/Ti_II3;
kI_II3=kc_II3*(1-Te/(Ti_II3));
alpha_II3=kI_II3/kP_II3;
Be_II3=5;
Bde_II3=alpha_II3*Be_II3;
Bdu_II3=kI_II3*Be_II3;
% crearea unui nou fisier    
b_II3=newfis('regPI'); 
assignin('base','b',b_II3)
% adaugarea variabile lingvistice 

b_II3=addvar(b_II3,'input','e',[Be_II3 5*Be_II3]);
b_II3=addmf(b_II3,'input',1,'N','trapmf',[-5*Be_II3 -5*Be_II3 -Be_II3 0]);
b_II3=addmf(b_II3,'input',1,'ZE','trimf',[-Be_II3  Be_II3]); 
b_II3=addmf(b_II3,'input',1,'P','trapmf',[0 Be_II3 5*Be_II3 5*Be_II3]);
%plotmf(b_II3,'input',1), grid
%pause

b_II3=addvar(b_II3,'input','de',[Bde_II3 5*Bde_II3]);
b_II3=addmf(b_II3,'input',2,'N','trapmf',[-5*Bde_II3 -5*Bde_II3 -Bde_II3 0]);
b_II3=addmf(b_II3,'input',2,'ZE','trimf',[-Bde_II3 Bde_II3]); 
b_II3=addmf(b_II3,'input',2,'P','trapmf',[0 Bde_II3 5*Bde_II3 5*Bde_II3]);
%plotmf(b_II3,'input',2), grid
%pause

b_II3=addvar(b_II3,'output','du',[-5*Bdu_II3 5*Bdu_II3]);
b_II3=addmf(b_II3,'output',1,'NB','trimf',[-2*Bdu_II3-(Bdu_II3/10) -2*Bdu_II3 -2*Bdu_II3+(Bdu_II3/10)]);
b_II3=addmf(b_II3,'output',1,'NS','trimf',[-Bdu_II3-(Bdu_II3/10) -Bdu_II3 -Bdu_II3+(Bdu_II3/10)]);
b_II3=addmf(b_II3,'output',1,'ZE','trimf',[-Bdu_II3/10 0 Bdu_II3/10]);
b_II3=addmf(b_II3,'output',1,'PS','trimf',[Bdu_II3-(Bdu_II3/10) Bdu_II3 Bdu_II3+(Bdu_II3/10)]);
b_II3=addmf(b_II3,'output',1,'PB','trimf',[2*Bdu_II3-(Bdu_II3/10) 2*Bdu_II3 2*Bdu_II3+(Bdu_II3/10)]);
%plotmf(b_II3,'output',1), grid
%pause

regLista=[1 1 1 1 1
    1 2 2 1 1
    1 3 3 1 1
    2 1 2 1 1
    2 2 3 1 1
    2 3 4 1 1
    3 1 3 1 1 
    3 2 4 1 1
    3 3 5 1 1];
b_II3=addRule(b_II3,regLista); 
writeFIS(b_II3,'regPI'); 