# SOLUȚII DE REGLARE PENTRU UN MODEL OPTIMIZAT AL UNUI SISTEM DE ACȚIONARE ELECTRICĂ CU PARAMETRI VARIABILI

## Link repository:

https://gitlab.upt.ro/nixandra.iancu/lucrare-licenta.git

## Tools

- Matlab/Simulink

## Test and Deploy


- ruleaza apel.m si dupa simuleaza modelul mcc_sra_c_reg.mdl pentru a vedea graficul solutiilor de reglare
- pentru solutii de optimizare se ruleaza unul dintre algoritmii din fisierul optimizare_SWS.m

## Autor
Nixandra-Loredana Iancu
nixandra.iancu@student.upt.ro


