% Lower and upper bounds
lb = [0.001 0.001];
ub = [100 1000];

% Number of wolves
num_wolves = 20;

% Maximum number of iterations
max_iter = 100;

% Run Grey Wolf Optimizer
[thetha, fval] = greyWolfOptimizer(@(thetha) functie_SWS_PI_opt(thetha,N), lb, ub, num_wolves, max_iter);

% Display results
disp(['Best solution: ', num2str(thetha)]);
disp(['Best fitness: ', num2str(fval)]);