% Parametrii regulatorului Fuzzy
kr_IE3=1/(kPC*Ts);
Tr_IE3=Tm;
kc_IE3=kr_IE3*Tr_IE3;
Ti_IE3=Tr_IE3;
kI_IE3=((kc_IE3)/Ti_IE3)/5;
kP_IE3=(kc_IE3*(1-Te/(Ti_IE3)))/5;
alpha_IE3=kI_IE3/kP_IE3;
Be_IE3=5;
Bde_IE3=5*alpha_IE3*Be_IE3;
Bdu_IE3=kI_IE3*Be_IE3;
% crearea unui nou fisier    
b_IE3=newfis('regPI'); 
assignin('base','b',b_IE3)
% adaugarea variabile lingvistice 
b_IE3=addvar(b_IE3,'input','e',[Be_IE3 5*Be_IE3]);
b_IE3=addmf(b_IE3,'input',1,'N','trapmf',[-5*Be_IE3 -5*Be_IE3 -Be_IE3 0]);
b_IE3=addmf(b_IE3,'input',1,'Z','trimf',[-Be_IE3 Be_IE3]); 
b_IE3=addmf(b_IE3,'input',1,'P','trapmf',[0 Be_IE3 5*Be_IE3 5*Be_IE3]);
%plotmf(b_IE3,'input',1), grid
%pause

b_IE3=addvar(b_IE3,'input','de',[Bde_IE3 5*Bde_IE3]);
b_IE3=addmf(b_IE3,'input',2,'N','trapmf',[-5*Bde_IE3 -5*Bde_IE3 -Bde_IE3 0]);
b_IE3=addmf(b_IE3,'input',2,'Z','trimf',[-Bde_IE3 0 Bde_IE3]); 
b_IE3=addmf(b_IE3,'input',2,'P','trapmf',[0 Bde_IE3 5*Bde_IE3 5*Bde_IE3]);
%plotmf(b_IE3,'input',2), grid
%pause

b_IE3=addvar(b_IE3,'output','du',[-5*Bdu_IE3 5*Bdu_IE3]);
b_IE3=addmf(b_IE3,'output',1,'NB','trimf',[-2*Bdu_IE3-(Bdu_IE3/10) -2*Bdu_IE3 -2*Bdu_IE3+(Bdu_IE3/10)]);
b_IE3=addmf(b_IE3,'output',1,'NS','trimf',[-Bdu_IE3-(Bdu_IE3/10) -Bdu_IE3 -Bdu_IE3+(Bdu_IE3/10)]);
b_IE3=addmf(b_IE3,'output',1,'ZE','trimf',[-Bdu_IE3/10 0 Bdu_IE3/10]);
b_IE3=addmf(b_IE3,'output',1,'PS','trimf',[Bdu_IE3-(Bdu_IE3/10) Bdu_IE3 Bdu_IE3+(Bdu_IE3/10)]);
b_IE3=addmf(b_IE3,'output',1,'PB','trimf',[2*Bdu_IE3-(Bdu_IE3/10) 2*Bdu_IE3 2*Bdu_IE3+(Bdu_IE3/10)]);
%plotmf(b_IE3,'output',1), grid
%pause
regLista=[1 1 1 1 1
    1 2 2 1 1
    1 3 3 1 1
    2 1 2 1 1
    2 2 3 1 1
    2 3 4 1 1
    3 1 3 1 1 
    3 2 4 1 1
    3 3 5 1 1];

b_IE3=addRule(b_IE3,regLista); 
writeFIS(b_IE3,'regPI'); 
