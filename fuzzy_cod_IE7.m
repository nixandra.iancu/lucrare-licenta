% Parametrii regulatorului Fuzzy
kr_IE7=1/(kPC*Ts);
Tr_IE7=Tm;
kc_IE7=kr_IE7*Tr_IE7;
Ti_IE7=Tr_IE7;
kI_IE7=((kc_IE7)/Ti_IE7)/5;
kP_IE7=(kc_IE7*(1-Te/(Ti_IE7)))/5;
alpha_IE7=kI_IE7/kP_IE7;
Be_IE7=5;
Bde_IE7=5*alpha_IE7*Be_IE7;
Bdu_IE7=kI_IE7*Be_IE7;
% crearea unui nou fisier    
b_IE7=newfis('regPI'); 
assignin('base','b',b_IE7)

% adaugarea variabile lingvistice 
b_IE7=addvar(b_IE7,'input','e',[-4*Be_IE7 4*Be_IE7]);
b_IE7=addmf(b_IE7,'input',1,'GN','trapmf',[-4*Be_IE7 -4*Be_IE7 -3*Be_IE7 -2*Be_IE7]); 
b_IE7=addmf(b_IE7,'input',1,'MN','trimf',[-3*Be_IE7 -2*Be_IE7 -Be_IE7]); 
b_IE7=addmf(b_IE7,'input',1,'PN','trimf',[-2*Be_IE7 -Be_IE7]); 
b_IE7=addmf(b_IE7,'input',1,'ZZ','trimf',[-Be_IE7 0 Be_IE7]); 
b_IE7=addmf(b_IE7,'input',1,'PP','trimf',[0 Be_IE7 2*Be_IE7]); 
b_IE7=addmf(b_IE7,'input',1,'MP','trimf',[Be_IE7 2*Be_IE7 3*Be_IE7]); 
b_IE7=addmf(b_IE7,'input',1,'GP','trapmf',[2*Be_IE7 3*Be_IE7 4*Be_IE7 4*Be_IE7]); 
%plotmf(b_IE7,'input',1), grid
%pause

b_IE7=addvar(b_IE7,'input','de',[-4*Bde_IE7 4*Bde_IE7]);
b_IE7=addmf(b_IE7,'input',2,'GN','trapmf',[-4*Bde_IE7 -4*Bde_IE7 -3*Bde_IE7 -2*Bde_IE7]); 
b_IE7=addmf(b_IE7,'input',2,'MN','trimf',[-3*Bde_IE7 -2*Bde_IE7 -Bde_IE7]); 
b_IE7=addmf(b_IE7,'input',2,'PN','trimf',[-2*Bde_IE7 -Bde_IE7 0]); 
b_IE7=addmf(b_IE7,'input',2,'ZZ','trimf',[-Bde_IE7 0 Bde_IE7]);
b_IE7=addmf(b_IE7,'input',2,'PP','trimf',[0 Bde_IE7 2*Bde_IE7]); 
b_IE7=addmf(b_IE7,'input',2,'MP','trimf',[Bde_IE7 2*Bde_IE7 3*Bde_IE7]); 
b_IE7=addmf(b_IE7,'input',2,'GP','trapmf',[2*Bde_IE7 3*Bde_IE7 4*Bde_IE7 4*Bde_IE7]); 
%plotmf(b_IE7,'input',2), grid
%pause

b_IE7=addvar(b_IE7,'output','du',[-4*Bdu_IE7 4*Bdu_IE7]);
b_IE7=addmf(b_IE7,'output',1,'GN','trapmf',[-4*Bdu_IE7 -4*Bdu_IE7 -3*Bdu_IE7 -2*Bdu_IE7]);
b_IE7=addmf(b_IE7,'output',1,'MN','trimf',[-3*Bdu_IE7 -2*Bdu_IE7 -Bdu_IE7]);
b_IE7=addmf(b_IE7,'output',1,'PN','trimf',[-2*Bdu_IE7 -Bdu_IE7 0]);
b_IE7=addmf(b_IE7,'output',1,'ZZ','trimf',[-Bdu_IE7 0 Bdu_IE7]);
b_IE7=addmf(b_IE7,'output',1,'PP','trimf',[0 Bdu_IE7 2*Bdu_IE7]);
b_IE7=addmf(b_IE7,'output',1,'MP','trimf',[Bdu_IE7 2*Bdu_IE7 3*Bdu_IE7]);
b_IE7=addmf(b_IE7,'output',1,'GP','trapmf',[2*Bdu_IE7 3*Bdu_IE7 4*Bdu_IE7 4*Bdu_IE7]);
%plotmf(b_IE7,'output',1), grid
%pause

% crearea listei de reguli
regLista=[1 1 1 1 1
          2 1 1 1 1
          3 1 2 1 1
          4 1 2 1 1
          5 1 3 1 1
          6 1 3 1 1
          7 1 4 1 1
          1 2 1 1 1
          2 2 2 1 1
          3 2 2 1 1
          4 2 3 1 1
          5 2 3 1 1
          6 2 4 1 1
          7 2 5 1 1
          1 3 2 1 1
          2 3 2 1 1
          3 3 3 1 1
          4 3 3 1 1
          5 3 4 1 1
          6 3 5 1 1
          7 3 5 1 1
          1 4 2 1 1
          2 4 3 1 1
          3 4 3 1 1
          4 4 4 1 1
          5 4 5 1 1
          6 4 5 1 1
          7 4 6 1 1
          1 5 3 1 1
          2 5 3 1 1
          3 5 4 1 1
          4 5 5 1 1
          5 5 5 1 1
          6 5 6 1 1
          7 5 6 1 1
          1 6 3 1 1
          2 6 4 1 1
          3 6 5 1 1
          4 6 5 1 1
          5 6 6 1 1
          6 6 6 1 1
          7 6 7 1 1
          1 7 4 1 1
          2 7 5 1 1
          3 7 5 1 1
          4 7 6 1 1
          5 7 6 1 1  
          6 7 7 1 1
          7 7 7 1 1]; 
b_IE7=addRule(b_IE7,regLista); 
writeFIS(b_IE7,'regPI'); 
